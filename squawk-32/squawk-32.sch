EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:squawk-32-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R4
U 1 1 5AA143FB
P 4750 2200
F 0 "R4" V 4830 2200 50  0000 C CNN
F 1 "R" V 4750 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4680 2200 50  0001 C CNN
F 3 "" H 4750 2200 50  0001 C CNN
	1    4750 2200
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR01
U 1 1 5AA14457
P 5300 3200
F 0 "#PWR01" H 5300 3050 50  0001 C CNN
F 1 "+3V3" H 5300 3340 50  0000 C CNN
F 2 "" H 5300 3200 50  0001 C CNN
F 3 "" H 5300 3200 50  0001 C CNN
	1    5300 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AA14492
P 4900 1700
F 0 "#PWR02" H 4900 1450 50  0001 C CNN
F 1 "GND" H 4900 1550 50  0000 C CNN
F 2 "" H 4900 1700 50  0001 C CNN
F 3 "" H 4900 1700 50  0001 C CNN
	1    4900 1700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 5AA147D2
P 4500 1400
F 0 "#PWR03" H 4500 1250 50  0001 C CNN
F 1 "+5V" H 4500 1540 50  0000 C CNN
F 2 "" H 4500 1400 50  0001 C CNN
F 3 "" H 4500 1400 50  0001 C CNN
	1    4500 1400
	1    0    0    -1  
$EndComp
Text Label 4300 2100 0    60   ~ 0
IO-12
$Comp
L Conn_01x02 J5
U 1 1 5AA15164
P 4700 1400
F 0 "J5" H 4700 1500 50  0000 C CNN
F 1 "Conn_01x02" H 4700 1200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 4700 1400 50  0001 C CNN
F 3 "" H 4700 1400 50  0001 C CNN
	1    4700 1400
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J6
U 1 1 5AA15212
P 5300 2000
F 0 "J6" H 5300 2200 50  0000 C CNN
F 1 "Conn_01x03" H 5300 1800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5300 2000 50  0001 C CNN
F 3 "" H 5300 2000 50  0001 C CNN
	1    5300 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AA1537D
P 2850 3200
F 0 "#PWR04" H 2850 2950 50  0001 C CNN
F 1 "GND" H 2850 3050 50  0000 C CNN
F 2 "" H 2850 3200 50  0001 C CNN
F 3 "" H 2850 3200 50  0001 C CNN
	1    2850 3200
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J2
U 1 1 5AA1547A
P 2150 3050
F 0 "J2" H 2150 3250 50  0000 C CNN
F 1 "Conn_01x03" H 2150 2850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2150 3050 50  0001 C CNN
F 3 "" H 2150 3050 50  0001 C CNN
	1    2150 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 3200 3300 3200
Wire Wire Line
	4150 1400 4500 1400
Wire Wire Line
	4150 2000 5100 2000
Wire Wire Line
	4150 1900 5100 1900
Wire Wire Line
	4150 3200 5300 3200
Wire Wire Line
	5100 3200 5100 2100
Wire Wire Line
	5000 1900 5000 1700
Wire Wire Line
	5000 1700 4900 1700
Connection ~ 5000 1900
Connection ~ 4750 2000
Wire Wire Line
	4750 2350 5100 2350
Connection ~ 5100 2350
Wire Wire Line
	4750 2050 4750 2000
Wire Wire Line
	4500 1900 4500 1500
Connection ~ 4500 1900
Wire Wire Line
	4350 3500 4350 3200
Connection ~ 4350 3200
Wire Wire Line
	3000 3050 3000 3200
Wire Wire Line
	2350 3050 3000 3050
Connection ~ 3000 3200
Wire Wire Line
	3050 2950 2350 2950
Wire Wire Line
	2350 3150 2350 3500
Connection ~ 2350 3500
Wire Wire Line
	2450 2950 2450 2600
Connection ~ 2450 2950
$Comp
L R R2
U 1 1 5AA166F9
P 2300 2600
F 0 "R2" V 2380 2600 50  0000 C CNN
F 1 "R" V 2300 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2230 2600 50  0001 C CNN
F 3 "" H 2300 2600 50  0001 C CNN
	1    2300 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 2600 1950 2600
Wire Wire Line
	1950 2600 1950 3500
Connection ~ 1950 3500
$Comp
L Conn_01x19 J3
U 1 1 5AA1693F
P 3500 2300
F 0 "J3" H 3500 3300 50  0000 C CNN
F 1 "Conn_01x19" H 3500 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x19_Pitch2.54mm" H 3500 2300 50  0001 C CNN
F 3 "" H 3500 2300 50  0001 C CNN
	1    3500 2300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x19 J4
U 1 1 5AA16A1D
P 3950 2300
F 0 "J4" H 3950 3300 50  0000 C CNN
F 1 "Conn_01x19" H 3950 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x19_Pitch2.54mm" H 3950 2300 50  0001 C CNN
F 3 "" H 3950 2300 50  0001 C CNN
	1    3950 2300
	-1   0    0    1   
$EndComp
Connection ~ 5100 3200
Connection ~ -250 2050
Wire Wire Line
	1950 3500 4350 3500
Wire Wire Line
	3050 2950 3050 2000
Wire Wire Line
	3050 2000 3300 2000
$EndSCHEMATC
