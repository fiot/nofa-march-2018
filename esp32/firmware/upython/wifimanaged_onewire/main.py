import wifimgr
import ujson as json
import urequests as requests
import time
import machine
import onewire,ds18x20

base_url='https://test.farmos.net/farm/sensor/listener/'
public_key='4cb5a5e494bbfa5937f1a1e9363bdee3'
private_key='7f0b405f23c649ad758b8d254576eaa3'

url=base_url+public_key+'?private_key='+private_key
    
headers = {'Content-type':'application/json', 'Accept':'application/json'}

def measure_1wire(pin):
    dat = machine.Pin(pin)
    ds = ds18x20.DS18X20(onewire.OneWire(dat))
    roms = ds.scan()
    ds.convert_temp()
    time.sleep_ms(750)
    temp=ds.read_temp(roms[0])
    print(temp)
    return temp

def post_data(temp):
    payload={"temp": str(temp)}
    r=requests.post(url,data=json.dumps(payload),headers=headers)
    print('Status:',r.status_code)
    r.close()
        
print("hello!")

while True:
    temp=measure_1wire(4)
    time.sleep(2)
    wlan = wifimgr.get_connection()
    print("sleeping ....")
    time.sleep(4)
    post_data(temp)
    print("sleeping ....")
    time.sleep(60)

